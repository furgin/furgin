## Interface: 71500
## Title: Furgin
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: GratsDataStore
## SavedVariablesPerCharacter: FurginDataStore
## Dependencies: DataStore

Furgin_Data.lua
Furgin.lua

Caerdon_Localization.lua
Caerdon_Core.lua

Furgin_Sets.lua
Furgin_SetsUpdates.lua
Furgin_SetsWowHead.lua
Furgin_Frame.lua
Furgin_Grats.lua
Furgin_Damage.lua
Furgin_Debug.lua
Furgin_Rotation.lua