local slots = {
    ["legs"] = { name = "Legs", slotName = "HeadSlot" },
    ["waist"] = { name = "Waist", slotName = "WaistSlot" },
    ["hands"] = { name = "Hands", slotName = "HandsSlot" },
    ["chest"] = { name = "Chest", slotName = "ChestSlot" },
    ["feet"] = { name = "Feet", slotName = "FeetSlot" },
    ["wrist"] = { name = "Wrist", slotName = "WristSlot" },
    ["head"] = { name = "Head", slotName = "HeadSlot" },
    ["legs"] = { name = "Legs", slotName = "LegsSlot" },
    ["shoulder"] = { name = "Shoulder", slotName = "ShoulderSlot" }
}
local AllSets = {}
local AllItems = {}
local ROW_HEIGHT = 32
local MAX_ROWS = 13
local SetBrowserFrame
local SetsForCharacter
local SetBrowserRows = {}

local SetTypes = {
    ["HUNTER"] = "Mail",
    ["WARLOCK"] = "Cloth",
    ["PRIEST"] = "Cloth",
    ["PALADIN"] = "Plate",
    ["MAGE"] = "Cloth",
    ["ROGUE"] = "Leather",
    ["DRUID"] = "Leather",
    ["SHAMAN"] = "Mail",
    ["WARRIOR"] = "Plate",
    ["DEATHKNIGHT"] = "Plate",
    ["MONK"] = "Leather",
}

local function _GetSet(id)
    for _, s in ipairs(AllSets) do
        if type(id) == "number" then
            if s.id == id then
                return s
            end
        elseif type(id) == "string" then
            if s.name == id then
                return s
            end
        end
    end
end

local function _AddSet(set)

	local found = false;
	for _,v in pairs(AllSets) do
		if v["name"] == set["name"] then
			found = true
			-- update set
		    Furgin:Log("Found duplicate set |chighlight1" .. set["name"])		
		
			for slot,_ in pairs(slots) do
				if v[slot] and set[slot] then
					for _,id in pairs(set[slot]) do
						table.insert(v[slot],id)
					end
				elseif set[slot] then
					v[slot] = set[slot]
				end
			end
			
			break
		end
	end

	if found == false then
		table.insert(AllSets, set)
	end
end

-- itemId (number) item id
-- returns a table containing all sets for the given itemId
local function _GetItemSets(itemId)
    local sets = AllItems[itemId]
    if sets == nil then
        sets = {}
        for _, set in ipairs(AllSets) do
            local found = false
            for k, slot in pairs(slots) do
                local items = set[k]
                if items ~= nil then
                    for _, i in ipairs(items) do
                        if i == itemId then
                            found = true
                        end
                    end
                end
                if found then
                    break
                end
            end
            if found then
                table.insert(sets, set)
            end
        end
		if itemId ~= nil then
			AllItems[itemId] = sets
		end
    end
    return sets
end

local character

-- itemId (number) item id
-- returns how many of the specified item is available to transmog
local function _GetItemCount(itemId)
    local res = 0
    if character == nil then
        character = DataStore:GetCharacter()
    end

    if character ~= nil then
        local containers = DataStore:GetContainers(character)
        if containers ~= nil then
            for containerName, container in pairs(containers) do
                if container.size ~= nil then
                    for slotID = 1, container.size do
                        local id = container.ids[slotID]
                        if id ~= nil and itemId == id then
                            res = res + 1
                        end
                    end
                end
                --p(container)
            end
            if IsEquippedItem(itemId) then
                res = res + 1
            end
        end
    end
    return res
end

local function _GetSetForCharacter(set)
    local haveSlots = {} -- key is slot, value is array of itemid
    local need = ""
    local needCount = 0
    local haveCount = 0
    local have = ""
    for k, slot in pairs(slots) do
        local total = 0
        local items = set[k]
        if items ~= nil then
            for i, itemId in ipairs(items) do
			
				local itemLink = "|Hitem:"..itemId
				local appearanceID, isCollected, sourceID, shouldRetry = Furgin:GetItemAppearance(itemId, itemLink)
			
                --local res = _GetItemCount(itemId)
                --if res > 0 then
				if isCollected then
                    if haveSlots[k] == nil then
                        haveSlots[k] = {}
                    end
                    table.insert(haveSlots[k], itemId)
					total = total + 1
				end
            end
            -- p(slot..": "..total)
            if total == 0 then
                if #need > 0 then
                    need = need .. ", "
                end
                need = need .. k
                needCount = needCount + 1
            else
                if #have > 0 then
                    have = have .. ", "
                end
                have = have .. k
                haveCount = haveCount + 1
            end
        end
    end

    local msg = "|chighlight1" .. set.name
    if needCount > 0 then
        need = need .. "."
        msg = msg .. " |chighlight2Need(" .. needCount .. "):|r " .. need
    end
    if haveCount > 0 then
        have = have .. "."
        msg = msg .. " |chighlight2Have(" .. haveCount .. "):|r " .. have
    end

    return {
        ["set"] = set,
        ["need"] = need,
        ["have"] = have,
        ["haveSlots"] = haveSlots,
        ["needCount"] = needCount,
        ["haveCount"] = haveCount,
        ["msg"] = msg,
    }
end

-- returns a table containing all sets and a count of which items
-- are needed for the current character
local function _GetSetsForCharacter(minNeedCount)
    local sets = {}
    local name, realm = UnitName("player")
    local class, classFileName = string.upper(UnitClass("player"))
    local SetType = SetTypes[class]

    for _, set in ipairs(AllSets) do
        local applySet = SetType == nil or set.type == SetType
        if set.classes then
            applySet = set.classes == class
        end

        if applySet then
            local addToSets = true
            local SetForCharacter = _GetSetForCharacter(set)
            if minNeedCount ~= nil and SetForCharacter.needCount <= minNeedCount then
                addToSets = false
            end
            if addToSets then
                table.insert(sets, SetForCharacter)
            end
        end
    end
    table.sort(sets,
        function(a, b)
            local aa, bb = a.needCount, b.needCount
            if a.haveCount == 0 then aa = aa * 10 end
            if b.haveCount == 0 then bb = bb * 10 end
            if aa < bb then return true else return false end
        end)
    Furgin:Log("Found |chighlight1" .. #sets .. "|r sets for |c" .. class .. name .. "|r.")
    return sets
end

local function _AddSetInfo(tooltip)
    local itemName, itemLink = tooltip:GetItem()
    if itemLink ~= nil then
        local itemString = string.match(itemLink, "item[%-?%d:]+")
        local _, _itemId, enchantId, jewelId1, jewelId2, jewelId3,
        jewelId4, suffixId, uniqueId, linkLevel, reforgeId = strsplit(":", itemString)
        local itemId = tonumber(_itemId)
        local sets = _GetItemSets(itemId)

        if #sets > 0 then
            tooltip:AddLine(" ")
            tooltip:AddLine("Sets:")
            for _, set in ipairs(sets) do
                local set2 = _GetSetForCharacter(set)
                tooltip:AddLine(set.name)
                tooltip:AddDoubleLine(Furgin:Colours("|cwhiteNeed: |chighlight1" .. set2.needCount),
                    Furgin:Colours("|cwhiteHave: |chighlight1" .. set2.haveCount))
            end
            tooltip:Show()
        end
    end
end

local function isempty(s)
    return s == nil or s == ''
end

local function CreateRow(i, list, container)

    local width = list:GetWidth() - 16
    local row = CreateFrame("Frame", "$parentRow" .. i, container)
    row:SetSize(width, ROW_HEIGHT)
    row:EnableMouse(true)
    if i == 1 then
        row:SetPoint("TOPLEFT", list, 8, 0)
    else
        row:SetPoint("TOPLEFT", SetBrowserRows[i - 1], "BOTTOMLEFT")
    end

    local lhs = CreateFrame("Frame", "$parentLHS" .. i, row)
    lhs:SetSize(width / 2 - 8, ROW_HEIGHT)
    lhs:SetPoint("LEFT", 8, 0)

    local name = lhs:CreateFontString("$parentRowName" .. i)
    name:SetFontObject(FriendsFont_Large)
    name:SetPoint("LEFT")

    local rhs = CreateFrame("Frame", "$parentRHS" .. i, row)
    rhs:SetSize(width / 2 - 8, ROW_HEIGHT)
    rhs:SetPoint("RIGHT", -8, 0)

    local need = rhs:CreateFontString("$parentRowNeed" .. i)
    need:SetFontObject(GameFontNormal)
    need:SetPoint("BOTTOMRIGHT", rhs, "RIGHT")
    local have = rhs:CreateFontString("$parentRowHave" .. i)
    have:SetFontObject(GameFontNormal)
    have:SetPoint("TOPRIGHT", rhs, "RIGHT")
    local complete = rhs:CreateFontString("$parentRowComplete" .. i)
    complete:SetFontObject(GameFontNormal)
    complete:SetPoint("RIGHT")

    row.texture = row:CreateTexture()
    row.texture:SetAllPoints(row)
    row.texture:SetTexture(0, 0, 0, 0)

    row:HookScript("OnEnter", function(self)
        self.texture:SetTexture(1, 1, 1, 0.1)

        if self.set ~= nil then
            GameTooltip:SetOwner(container, "ANCHOR_CURSOR")
            GameTooltip:SetText(self.set.set.name)
            GameTooltip:AddLine(" ")
            for k, slot in pairs(slots) do
                local needSlot = (self.set.haveSlots[k] == nil and self.set.set[k] ~= nil)
                if needSlot then
                    GameTooltip:AddLine(slot.name)
                    for _, itemId in ipairs(self.set.set[k]) do
                        local itemName, itemLink, itemRarity, itemLevel, itemMinLevel, itemType,
                        itemSubType, itemStackCount, itemEquipLoc, itemTexture, itemSellPrice =
                        GetItemInfo(itemId)
                        GameTooltip:AddLine(itemName, 1, 1, 1)
                    end
                    GameTooltip:AddLine(" ")
                end
            end
            GameTooltip:Show()
        end
    end)
    row:HookScript("OnLeave", function(self)
        self.texture:SetTexture(0, 0, 0, 0)
        GameTooltip:Hide()
    end)

    row.name = name
    row.need = need
    row.have = have
    row.complete = complete

    return row
end

local function _Update(ScrollFrame)
    if SetsForCharacter == nil then
        Furgin:Error("no sets loaded")
        return
    end
    local maxValue = #SetsForCharacter
    FauxScrollFrame_Update(ScrollFrame, maxValue, MAX_ROWS, ROW_HEIGHT)
    -- #1 is a reference to the scroll bar frame.
    -- #2 is the total number of data available to be shown.
    -- #3 is how many rows of data can be displayed at once.
    -- #4 is the height of each row.
    local offset = FauxScrollFrame_GetOffset(ScrollFrame)
    for i = 1, MAX_ROWS do
        local value = i + offset
        local row = SetBrowserRows[i]
        if value <= maxValue then
            row.name:SetText(SetsForCharacter[value].set.name)
            local need = SetsForCharacter[value].need
            local have = SetsForCharacter[value].have
            if isempty(need) then
                row.complete:SetText(Furgin:Colours("|cgreenComplete"))
                row.need:SetText("")
                row.have:SetText("")
            elseif isempty(have) then
                row.complete:SetText(Furgin:Colours("|credAll"))
                row.need:SetText("")
                row.have:SetText("")
            else
                row.complete:SetText("")
                row.need:SetText("Need: " .. need)
                row.have:SetText("Have: " .. have)
            end
            row.set = SetsForCharacter[value]
            row:Show()
        else
            row:Hide()
        end
    end
end

local function _OpenSetBrowser()
    if SetBrowserFrame == nil then
        SetBrowserFrame = Furgin:CreateFrame(-- width, height
            700, 500,
            -- title
            "Set Browser",
            -- fn
            function(container)
                local ScrollFrame = CreateFrame("ScrollFrame", "$parentScrollBar", container, "FauxScrollFrameTemplate")
                ScrollFrame:SetPoint("TOPLEFT", container, "BOTTOMLEFT", 0, MAX_ROWS * ROW_HEIGHT)
                ScrollFrame:SetPoint("BOTTOMRIGHT", -16, 0)
                ScrollFrame:SetHeight(MAX_ROWS * ROW_HEIGHT)
                ScrollFrame:SetScript("OnVerticalScroll", function(self, offset)
                    self.offset = math.floor(offset / ROW_HEIGHT + 0.5)
                    _Update(self)
                end)
                ScrollFrame:SetScript("OnShow", function(self)
                    _Update(self)
                end)
                for i = 1, MAX_ROWS do
                    SetBrowserRows[i] = CreateRow(i, ScrollFrame, container)
                end
            end)
    end
    SetsForCharacter = Furgin:GetSetsForCharacter()
    SetBrowserFrame:Show()
end

local function _ToggleBrowser()
    if SetBrowserFrame ~= nil and SetBrowserFrame:IsVisible() then
        SetBrowserFrame:Hide()
    else
        _OpenSetBrowser()
    end
end

Furgin:OnTooltipSetItem(_AddSetInfo)

Furgin:RegisterMethod("GetSetsForCharacter", _GetSetsForCharacter)
Furgin:RegisterMethod("GetItemCount", _GetItemCount)
Furgin:RegisterMethod("AddSet", _AddSet)
Furgin:RegisterMethod("GetSet", _GetSet)
Furgin:RegisterMethod("ToggleSetBrowser", _ToggleBrowser)
Furgin:RegisterCommand("sets", _ToggleBrowser)

Furgin:OnLoad(function(addon)
    Furgin:RegisterTimer(time() + 2, function()
        Furgin:Log("Use |cgreen/furgin sets|r to open the sets browser")
    end)
end)

-- ADD SET SELECTION BUTTON TO TRANSMOG UI

local SelectSetMenu = CreateFrame("Frame", "$parentSelectSetMenu", UIParent, "UIDropDownMenuTemplate")

local function n(o)
    if o == true then return "true" end
    if o == false then return "false" end
    return o and o or ""
end

local function GetLinkFromLocation(location)
    local player, bank, bags, voidStorage, slot, bag, tab, voidSlot = EquipmentManager_UnpackLocation(location)
    if voidStorage then
        return GetVoidItemHyperlinkString((tab - 1) * 80 + voidSlot)
    elseif not bags then
        return GetInventoryItemLink("player", slot)
    else
        return GetContainerItemLink(bag, slot)
    end
end

local function GetItemId(msg)
    local itemId
    for itemLink in msg:gmatch("|%x+|Hitem:.-|h.-|h|r") do
        itemId = itemLink:match("item:(%d+):")
    end
    return tonumber(itemId)
end

local function OnClickSelectSetMenu(v)
    Furgin:Log("OnClickSelectSetMenu")
    local set = v.value

    for k, slot in pairs(slots) do
        local items = set.haveSlots[k]
        local slotID = GetInventorySlotInfo(slot.slotName)
        if items ~= nil then
            local itemTable = {}
            GetInventoryItemsForSlot(slotID, itemTable, "transmogrify")
            for location in pairs(itemTable) do
                local player, bank, bags, voidStorage, s, bag, tab, voidSlot = EquipmentManager_UnpackLocation(location)

                local itemLink = GetLinkFromLocation(location)
                local itemId = GetItemId(itemLink)
                if itemId == items[1] then
                    Furgin:Log("apply " .. itemId .. " " .. itemLink .. " to " .. k .. " slot.")
                    if voidStorage then
                        UseVoidItemForTransmogrify(tab, voidSlot, slotID)
                    else
                        UseItemForTransmogrify(bag, s, slotID)
                    end
                    TransmogrifyConfirmationPopup.slot = nil
                end
            end

            TransmogrifyModelFrame:TryOn(items[1])

        else
            TransmogrifyModelFrame:UndressSlot(slotID)
        end
    end
end

local SelectSetButton = CreateFrame("Button", "$parentSelectSetButton", nil, "UIPanelButtonTemplate")
SelectSetButton:SetHeight(20)
SelectSetButton:SetWidth(100)
SelectSetButton:SetToplevel(true)
SelectSetButton:SetText("Sets")

local menu

Furgin:RegisterEvent("CHAT_MSG_LOOT", function(message, sender, ...)
    if sender == GetUnitName("player") then
        Furgin:Log("clearing transmog set list")
        menu = nil
    end
end)

local function OpenSelectSetMenu(self, level)
    Furgin:Log("OpenSelectSetMenu")
    if menu == nil then
        menu = {}
        local sets = Furgin:GetSetsForCharacter()
        for _, set in ipairs(sets) do
            if set.needCount == 0 then
                table.insert(menu, {
                    text = set.set.name,
                    value = set,
                    func = OnClickSelectSetMenu
                })
            end
        end
    end
    SelectSetMenu:SetPoint("TOPLEFT", SelectSetButton, "BOTTOMLEFT", 0, 24)
    EasyMenu(menu, SelectSetMenu, SelectSetMenu, 0, 0, "MENU");
end

SelectSetButton:SetScript("OnClick", OpenSelectSetMenu)

Furgin:RegisterEvent("TRANSMOGRIFY_OPEN", function(self, name, ...)
    -- layout the button frame to include the SeletSetButtonFrame
    SelectSetButton:SetParent(TransmogrifyFrameButtonFrame)
    TransmogrifyMoneyFrame:SetWidth(TransmogrifyMoneyFrame:GetWidth() - 100)
    SelectSetButton:SetPoint("TOPRIGHT", TransmogrifyApplyButton, "TOPLEFT", 0, 0)
    TransmogrifyMoneyFrame:SetPoint("TOPRIGHT", SelectSetButton, "TOPLEFT", 10, -6)
    TransmogrifyFrameButtonFrame:Show()
    SelectSetButton:Show()
end)

Furgin:RegisterEvent("TRANSMOGRIFY_CLOSE", function(self, name, ...)
    SelectSetButton:Hide()
end)