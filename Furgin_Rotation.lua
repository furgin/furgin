local message

Furgin:OnLoad(function(addon)
	local frame = Furgin:CreateWindow(1000,300,"MessageFrame")
	local message = frame:CreateFontString()

	isValid = message:SetFont("Fonts\\FRIZQT__.TTF", 64)
	if isValid then
		Furgin:Log("isValid")
	else
		Furgin:Log("isNotValid")
	end
--	message:SetText("Hello World")
	message:SetPoint("BOTTOM")
--mybutton:SetWidth(80)
--mybutton:SetHeight(22)
	

    frame:SetPoint("TOP")
	frame:Show()

    Furgin:RegisterTimer(time() + 2, function()	
		local class, classFileName = UnitClass("player")
		Furgin:Log("Load rotation for class: |c"..classFileName..class)
		Furgin:RegisterUpdate(function()
			--Furgin:Log("Rotation Update")
			if InCombatLockdown() then
				local class, classFileName = UnitClass("player")
				if classFileName == '1SHAMAN' then

					local bfBuff = UnitBuff("player", "Boulderfist")
					local ftBuff = UnitBuff("player", "Flametongue")
					local sbBuff = UnitBuff("player", "Stormbringer")
					-- Maintain Boulderfist
					if bfBuff == nil then
						message:SetText("Boulderfist")
					-- Maintain Flametongue
					elseif ftBuff == nil then
						message:SetText("Flametongue")
					elseif sbBuff ~= nil then
						message:SetText("Stormstrike")
					else
						message:SetText("")
					end
				end
			else
				message:SetText("")
			end
		end)
    end)
end)

